from flask import Flask, render_template
server = Flask(__name__)

@server.route("/")
def hello():
    print('Restarted, Steve')
    return render_template('index.html')

@server.route("/new_page")
def hello2():
    print('Restarted, Steve')
    return render_template('new_page.html')

if __name__ == "__main__":
    print('In main again 3')
    server.run(host='0.0.0.0')
